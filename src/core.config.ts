import {Injectable, OnInit} from "@angular/core";
import * as _ from 'lodash';
import {State} from "../core/state.lib";
import {LocalStorage} from '../core/storage.lib';

@Injectable()
export class Cfg implements OnInit {
    public dev: boolean = true;

    public profile: string = "npm-p";

    public url: string;

    public log: any = {npm: "debug"};

    public domain = "www.domain.com";
    public lang: string = "en";
    public nav = {prev: "", next: ""};
    private loc: Array<any> = [];

    constructor(private state: State, private ls: LocalStorage) {
        switch(this.profile) {
        case "npm-i":
            this.url = "https://integration.domain.com/ehealth/";
            break;
        case "npm-p":
            this.url = "https://pre.domain.com/ehealth/";
            break;
        case "dev":
            this.url = "http://localhost:3028/api/";
            break;
        case "prod":
            this.url = "https://app.domain.com/ehealth/";
            break;
        case "mylocal":
            this.url = "http://192.168.191.239:3028/api/";
            break;
        case "pre":
            this.url = "https://t2.domain.com/ehealth/";
        }

        let l0 = navigator.language;
        let l1 = navigator.userLanguage;

        if(l0) {
            this.lang = l0.split('-')[0];
        }
        else if(l1){
            this.lang = l1.split('-')[0];
        }

        if(!(this.lang === "en" || this.lang === "de")) {
            this.lang = "en";
        }

        this.state.chan.subscribe((msg) => this.messenger(msg));
    }

    navUpdate(url: string): void {
        const prev = this.nav.next;
        if(prev === url) {return};
        this.nav.prev = prev;
        this.nav.next = url;
        this.ls.setObject("loc", this.nav);
        const urls = ["/feedline", "/body", "/nutrition", "/wellbeing"];
        if(urls.indexOf(url) !== -1) {
            this.ls.setObject("lastLoc", url);
        }
    }

    lastLocGet(): string {
        const url = this.ls.getObject("lastLoc");
        const urls = ["/feedline", "/body", "/nutrition", "/wellbeing"];
        if(urls.indexOf(url) !== -1) {
            return url;
        }
        else {
            return "/body";
        }
    }

    messenger(msg) {
        let {id, method, stat, code, url} = msg;
        if(id === "nav") {
            this.navUpdate(url)
            console.log(this.nav);
        }
    }

    ngOnInit() {
    }
}

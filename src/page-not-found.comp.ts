import {Component} from "@angular/core";

const directives = [];

@Component({
    selector: "page-not-found",
    templateUrl: "../app/core/404.html",
    styleUrls: ["../app/core/core.css"],
    directives: directives,
    providers: []
})

export class PageNotFoundComp {

}

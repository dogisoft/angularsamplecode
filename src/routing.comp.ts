import {ModuleWithProviders} from "@angular/core";
import {Routes, RouterModule} from "@angular/router";
import {AuthServ} from "./auth.serv";
import {PageNotFoundComp} from "./page-not-found.comp";
import {HeaderComp} from "../header/header.comp";
import {HomeComp} from "../home/home.comp";
import {LoginComp} from "../user/login.comp";
import {LogoutComp} from "../user/logout.comp";
import {PassResetComp} from "../user/pass-reset.comp";
import {UserResetComp} from "../user/user-reset.comp";
import {RegisterComp} from "../user/register.comp";
import {ActivationComp} from "../user/activation.comp";
import {FeedlineComp} from "../feedline/feedline.comp";
import {BodyComp} from "../body/body.comp";
import {WellbeingComp} from "../wellbeing/wellbeing.comp";
import {NutritionComp} from "../nutrition/nutrition.comp";
import {AccountComp} from "../account/account.comp";

const routes: Routes = [
    {
        path: "",
        component: HomeComp,
        canActivate: [AuthServ],
        data: {
            title: " - Feedline"
        }
    },

    {
        path: "login",
        component: LoginComp,
        data: {
            title: " - Login"
        }
    },

    {
        path: "reset-password",
        component: PassResetComp,
        data: {
            title: ""
        }
    },

    {
        path: "reset-username",
        component: UserResetComp,
        data: {
            title: ""
        }
    },

    {
        path: "register",
        component: RegisterComp,
        data: {
            title: ""
        }
    },

    {
        path: "activation",
        component: ActivationComp,
        data: {
            title: ""
        }
    },

    {
        path: "logout",
        component: LogoutComp,
        data: {
            title: ""
        }
    },

    {
        path: "feedline",
        component: FeedlineComp,
        canActivate: [AuthServ],
        data: {
            title: " - Feedline"
        }
    },

    {
        path: "body",
        component: BodyComp,
        data: {
            title: ""
        }
    },

    {
        path: "wellbeing",
        component: WellbeingComp,
        data: {
            title: " - Wellbeing"
        }
    },

    {
        path: "nutrition",
        component: NutritionComp,
        data: {
            title: " - Wellbeing"
        }
    },

    {
        path: "account",
        component: AccountComp,
        data: {
            title: " - Account"
        }
    },

    {
        path: "**",
        component: PageNotFoundComp
    }
];

export const appRoutingProviders: any[] = [
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes);

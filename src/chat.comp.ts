import {Component, OnInit, OnDestroy} from "@angular/core";
import {Cfg} from "../core/core.config";
import {AuthServ} from "../core/auth.serv";

declare var $zopim;

@Component({
    selector: "chat",
    template: ``
})

export class ChatComp implements OnInit, OnDestroy {
    constructor(private cfg: Cfg,
                private authServ: AuthServ) {
    }

    ngOnInit() {
        window["$zopim"] || (function(d, s){
            var z = window["$zopim"] = function(c){
                z["_"].push(c)
            },
            $ = z["s"] = d.createElement(s),
            e = d.getElementsByTagName(s)[0];
            z["set"] = function(o){
                z["set"]._.push(o)
            };
            z["_"] = [];
            z["set"]._ = [];
            $.async = !0;
            $.setAttribute("charset", "utf-8");
            $.setAttribute("id", "zopimScript");
            $.src = "//v2.zopim.com/?3rLtoslcw0elKjuNepeZr069ElkHWYRm";
            z["t"] = +new Date;
            $.type="text/javascript";
            e.parentNode.insertBefore($, e)
        })(document,"script");

        $zopim(() => {
            $zopim.livechat.setName(this.authServ.cred.username);

            $zopim.livechat.window.setSize("large");

            const lang = this.cfg.lang === "de" ? "de" : "en";

            $zopim.livechat.setLanguage(lang);

            if(lang === "de") {
                $zopim.livechat.bubble.setTitle("Fragen?");

                $zopim.livechat.bubble.setText("Hier klicken, um zu chatten.");

                $zopim.livechat.setGreetings({
                    online: "Chatte mit uns",
                    offline: "Hinterlasse eine Nachricht"
                });

                $zopim.livechat.window.setTitle("Humanoo Live Chat");

                $zopim.livechat.concierge.setName("Unsere Experten");

                $zopim.livechat.concierge.setTitle("Deine Fragen an uns...");
            }
        });
    }

    ngOnDestroy() {
        window["$zopim"] = undefined;
        $(".zopim").remove();
        $("#zopimScript").remove();
        $(".jx_ui_StyleSheet").remove();
    }
}
